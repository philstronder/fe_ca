
const onMenuClick = (e) => {
    clearAllActiveMenuLinks();
    const divContainer = e.target.parentElement;
    divContainer.classList.add('active');

    const target = e.target.innerText.toLowerCase();

    if(target) {
        document.getElementById('content').innerHTML = '<object id="content-object" type="text/html" data="' + target + '.html" ></object>';
    }

    document.getElementById('content-object').setAttribute('style', 'width: 100%; height: 400px;');
}

const clearAllActiveMenuLinks = () => {
    const menu = document.querySelectorAll('.header--menu ul li');

    menu.forEach(item => {
        item.children[0].classList.remove('active');
    });
}

const addEventHandlers = () => {
    const menuLinks = document.querySelectorAll('div.header--menu-link a');
    if(!!menuLinks) {
        menuLinks.forEach(item => {
            item.addEventListener('click', () => {
                onMenuClick(event);
            });
        })
    }
}

const populateFields = () => {
    document.querySelector('.header--profile-info.name').innerText = localStorage.getItem('name');
    document.querySelector('.header--profile-info.location').innerText = localStorage.getItem('location');
    document.querySelector('.header--profile-info.phone').innerText = localStorage.getItem('phone');
}

populateFields();
addEventHandlers();


//////////////////////
//    Mobile scripts
/////////////////////

const mobileSize = 980;

const formatHeaderProfileInfo = () => {
    if(window.innerWidth <= mobileSize) {
        const infos = document.querySelectorAll('.header--profile-info .container');
        
        if(!!infos) {
            const infoContainer = document.querySelector('.header--profile-info');
            infoContainer.appendChild(infos[0]);
        }
    }
}

formatHeaderProfileInfo();