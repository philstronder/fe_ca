const mobileSize = 680;


const addEventHandlers = () => {
    addCancelClickHandler();
    addSaveClickHandler();
    addPencilClickHandler();
}

const addPencilClickHandler = () => {
    const pencils = document.querySelectorAll('ion-icon[name="pencil"]');

    if(pencils) {
        pencils.forEach(item => {
            item.addEventListener('click', (e) => {
                const el = e.target;
                
                //hides all forms
                const allForms = document.querySelectorAll('.about--form form');
                allForms.forEach(form => {
                    form.setAttribute('style', 'display: none;');
                });
                
                //Desktop
                if(window.innerWidth > mobileSize) {
                    //displays div which contains all forms
                    const frm = document.querySelector('.about--form');
                    const top = el.parentElement.offsetTop + 15;
                    const left = el.parentElement.offsetLeft + 100;
                    frm.setAttribute('style', 'display: inline; top: ' + top + '; left: ' + left);
                } else { //Mobile
                    //hides user data
                    document.querySelector('.about--personal-details').setAttribute('style', 'display: none');

                    //displays div which contains all forms
                    const frm = document.querySelector('.about--form');
                    const top = el.parentElement.offsetTop + 61;
                    const left = 0;
                    const display = 'display: inline; top: ' + top + '; left: ' + left + '; width: 100%; height: 100%;';
                    frm.setAttribute('style', display);

                    //hides the pencil icon
                    item.setAttribute('style', 'display: none;');
                }

                //chooses which form to display
                const frmType = el.getAttribute('frm');
                const frmToShow = document.getElementById(frmType);

                //fills the form fields to edit
                switch(frmType){
                    case 'mobile':
                        document.querySelector('form#MOBILE input[placeholder="NAME"]').value = localStorage.getItem('name');
                        document.querySelector('form#MOBILE input[placeholder="PHONE"]').value = localStorage.getItem('phone');
                        document.querySelector('form#MOBILE input[placeholder="WEBSITE"]').value = localStorage.getItem('website');
                        document.querySelector('form#MOBILE input[placeholder="LOCATION"]').value = localStorage.getItem('location');
                        break;  
                    case 'name':
                        document.querySelector('form#NAME input[placeholder="NAME"]').value = localStorage.getItem('name');
                        break;
                    case 'phone':
                        document.querySelector('form#PHONE input[placeholder="PHONE"]').value = localStorage.getItem('phone');
                        break;
                    case 'website':
                        document.querySelector('form#WEBSITE input[placeholder="WEBSITE"]').value = localStorage.getItem('website');
                        break;
                    case 'location':
                        document.querySelector('form#LOCATION input[placeholder="LOCATION"]').value = localStorage.getItem('location');
                        break;
                }

                frmToShow.setAttribute('style', 'display: inline;');
            });
        });
    }
}

const addCancelClickHandler = () => {
    const btnCancel = document.querySelectorAll('.about--button-cancel');

    btnCancel.forEach(item => {
        item.addEventListener('click', (e) => {
            e.preventDefault();
            if(item.form){
                item.form.querySelector('input').value = '';
                item.form.parentElement.setAttribute('style', 'display: none;');
            }

            //Mobile
            if(window.innerWidth <= mobileSize) {
                //displays the pencil icon
                const editIcon = document.querySelector('.about--pencil-mobile').children[0];
                editIcon.setAttribute('style', 'display: block;');
            }

            //shows user data
            document.querySelector('.about--personal-details').setAttribute('style', 'display: inline');
        });
    });    
}

const addSaveClickHandler = () => {
    const btnSave = document.querySelectorAll('.about--button-save');

    btnSave.forEach(item => {
        item.addEventListener('click', (e) => {
            e.preventDefault();

            //Desktop
            if(window.innerWidth > mobileSize) {
                const input = item.form.querySelector('input');
                const value = input.value;
                
                if (typeof(Storage) !== 'undefined') {    
                    localStorage.setItem(item.form.id, value);
                }

                populateFields();
                
                item.form.querySelector('input').value = '';
            }
            else {
                //Mobile
                const name = item.form.querySelector('input[placeholder="NAME"]').value;
                const phone = item.form.querySelector('input[placeholder="PHONE"]').value;
                const website = item.form.querySelector('input[placeholder="WEBSITE"]').value;
                const location = item.form.querySelector('input[placeholder="LOCATION"]').value;

                if (typeof(Storage) !== 'undefined') {    
                    localStorage.setItem('name', name);
                    localStorage.setItem('phone', phone);
                    localStorage.setItem('website', website);
                    localStorage.setItem('location', location);
                }

                item.form.querySelectorAll('input').value = '';

                populateFields();

                //displays the pencil icon
                const editIcon = document.querySelector('.about--pencil-mobile').children[0];
                editIcon.setAttribute('style', 'display: block;');
            }

            item.form.parentElement.setAttribute('style', 'display: none;');
            document.querySelector('.about--personal-details').setAttribute('style', 'display: inline');
        });
    });
}

const loadInitialData = () => {
    if (typeof(Storage) !== 'undefined') {
         if(!localStorage.getItem('name'))
             localStorage.setItem('name', 'Jessica Parker');  
         if(!localStorage.getItem('location'))
             localStorage.setItem('location', 'Newport Beach, CA')  ;
         if(!localStorage.getItem('phone'))
             localStorage.setItem('phone', '(949) 325 - 68594');  
         if(!localStorage.getItem('website'))    
             localStorage.setItem('website', 'www.seller.com');
    }
}

const populateFields = () => {
    if (typeof(Storage) !== 'undefined') {
        document.querySelector('.about--content-name').innerText = localStorage.getItem('name');
        document.querySelector('.about--content-phone').innerText = localStorage.getItem('phone');
        document.querySelector('.about--content-website').innerText = localStorage.getItem('website');
        document.querySelector('.about--content-location').innerText = localStorage.getItem('location');
    }
}

addEventHandlers();
loadInitialData();
populateFields();


